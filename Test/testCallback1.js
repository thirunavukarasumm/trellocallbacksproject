const callback1 = require("../callback1");

callback1("mcu453ed", null,(err, data) => {
    if (err) {
        if (err.code === "ENOENT") {
            console.log(`File doesn't exist - ${err.path}`);
        }
    } else {
        console.log(data);
    }
});

