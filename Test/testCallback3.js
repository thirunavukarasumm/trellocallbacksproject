const callback3 = require("../callback3");

callback3("qwsa221", (err, data) => {
    if (err) {
        if (err.code === "ENOENT") {
            console.log(`File doesn't exist - ${err.path}`);
        }
    } else {
        console.log(data);
    }
});

