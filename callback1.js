const fs = require("fs");
const path = require("path");

/* 
    Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

function boardInformation(boardID, villianName, callback) {
    let boardsFile = "./Data/boards.json"
    setTimeout(() => {
        fs.readFile(path.join(__dirname, boardsFile), (err, data) => {
            if (err) {
                callback(err);
            } else {
                let resultData = JSON.parse(data);
                if (villianName === null) {
                    callback(null, resultData.filter((villian) => {
                        return villian.id === boardID;
                    }));

                } else {
                    callback(null, resultData.filter((villian) => {
                        return villian["name"] === villianName;
                    }));
                }

            }
        })
    }, 2 * 1000);
}

module.exports = boardInformation;