const fs = require("fs");
const path = require("path");


/* 
    Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const callback1 = require("./callback1");
const callback2 = require("./callback2");
const callback3 = require("./callback3");

function getInformation(villainName, ...ability) {
    setTimeout(() => {
        
        callback1(null, villainName, (error, villianData) => {
            if (error) {
                if (error.code === "ENOENT") {
                    console.log(`File doesn't exist - ${error.path}`);
                }
            } else {
                if (villianData.length !== 0) {
                    callback2(villianData[0]["id"], (error, abilityData) => {
                        if (error) {
                            if (error.code === "ENOENT") {
                                console.log(`File doesn't exist - ${error.path}`);
                            }
                        } else {
                            let cardResult = {};
                            
                            let listdata = abilityData.filter((power) => {
                                return ability.includes(power.name)
                            });
                            
                            for (let index = 0; index < listdata.length; index++) {
                                callback3(listdata[index]["id"], (error, cardData) => {
                                    if (error) {
                                        if (error.code === "ENOENT") {
                                            console.log(`File doesn't exist - ${error.path}`);
                                        }
                                    } else {
                                        cardResult[listdata[index]["name"]] = cardData;
                                        if (index === listdata.length - 1) {
                                            console.log(villianData);
                                            console.log(abilityData);
                                            console.log(cardResult);
                                        }
                                    }
                                })
                            }


                        }
                    })
                } else {
                    console.log(`No data available on the provided villian - ${villainName}`);
                }

            }
        })
    }, 2 * 1000);
}


module.exports = getInformation;