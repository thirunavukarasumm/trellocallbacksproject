const fs = require("fs");
const path = require("path");


/* 
    Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const callback1 = require("./callback1");
const callback2 = require("./callback2");
const callback3 = require("./callback3");

function getInformation(villainName,) {
    setTimeout(() => {
        callback1(null, villainName, (error, villianData) => {
            if (error) {
                if (error.code === "ENOENT") {
                    console.log(`File doesn't exist - ${error.path}`);
                }
            } else {
                if (villianData.length !== 0) {
                    callback2(villianData[0]["id"], (error, abilityData) => {
                        if (error) {
                            if (error.code === "ENOENT") {
                                console.log(`File doesn't exist - ${error.path}`);
                            }
                        } else {
                            let cardResult = {};
                            for (let index = 0; index < abilityData.length; index++) {
                                callback3(abilityData[index]["id"], (error, cardData) => {
                                    if (error) {
                                        if (error.code === "ENOENT") {
                                            console.log(`File doesn't exist - ${error.path}`);
                                        }
                                    } else {
                                        cardResult[abilityData[index]["name"]] = cardData;
                                        if (index === abilityData.length - 1) {
                                            console.log(villianData);
                                            console.log(abilityData);
                                            console.log(cardResult);
                                        }
                                    }
                                })
                            }
                        }
                    })
                } else {
                    console.log(`No data available on the provided villian - ${villainName}`);
                }

            }
        })
    }, 2 * 1000);
}


module.exports = getInformation;