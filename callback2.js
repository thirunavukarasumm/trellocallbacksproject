const fs = require("fs");
const path = require("path");

/* 
    Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/



function listInformation(boardID, callback) {
    let listFile = "./Data/lists.json"
    setTimeout(() => {
        fs.readFile(path.join(__dirname, listFile), (err, data) => {
            if (err) {
                callback(err);
            } else {
                let resultData = JSON.parse(data)[boardID];
                if (resultData === undefined) {
                    resultData = {};
                }  
                callback(null, resultData);
            }
        })
    }, 2 * 1000);
}

module.exports = listInformation;