const fs = require("fs");
const path = require("path");

/* 
    Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

function cardInformation(listID, callback) {
    let cardFile = "./Data/cards.json"
    setTimeout(() => {
        fs.readFile(path.join(__dirname, cardFile), (err, data) => {
            if (err) {
                callback(err);
            } else {
                let resultData = JSON.parse(data)[listID];
                if (resultData === undefined){  
                    resultData = {};
                }   
                callback(null, resultData);
            }
        })
    }, 2 * 1000);
}

module.exports = cardInformation;